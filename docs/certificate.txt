openssl genrsa -out qa-ingress-sfs.key 2048
openssl req -new -key qa-ingress-sfs.key -out qa-ingress-sfs.csr -subj "/CN=sfs.qa.oecd.redpelicans.com"
openssl x509 -req -days 365 -in qa-ingress-sfs.csr -signkey qa-ingress-sfs.key -out qa-ingress-sfs.crt
kubectl create secret tls sfs-qa-cert-secret --cert qa-ingress-sfs.crt --key qa-ingress-sfs.key -n qa
