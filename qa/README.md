# dive in
```
gcloud config set project oecd-228113
gcloud config set container/cluster oecd
gcloud config set compute/region europe-west1-b
gcloud container clusters get-credentials oecd --region europe-west1-b
kubectl get pods -n qa -o wide
```

# solr
1. apply solr init `kubectl apply -f solr.init.yaml`
1. enter the container `kubectl exec -it solr-<hash> -n qa -- bash`
1. create the core `solr create sdmx-facet-search`
1. move the solr file `mv opt/solr/server/solr/*  /data`
1. note: /data should contains files and at least solr.xml
1. remove the worker
1. apply solr `kubectl apply -f solr.yaml`

# postgres with helm
1. `helm repo add bitnami https://charts.bitnami.com/bitnami`
1. `helm install postgres -f postgres/values.yaml bitnami/postgresql -n=qa`

# keycloak with helm
1. add codecentric repo:
  `helm repo add codecentric https://codecentric.github.io/helm-charts`
1. install keycloak chart (name is important):
  `helm install keycloak -f keycloak/values.yaml codecentric/keycloak --namespace=qa`
1. disable ssl:
```
$ kubectl exec -it keycloak-0 -- bash
$$ cd keycloak/bin
$$ ./kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user admin 
Logging into http://localhost:8080/auth as user admin of realm master
Enter password: admin
$$ ./kcadm.sh update realms/master -s sslRequired=NONE
```
