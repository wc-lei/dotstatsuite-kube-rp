kubectl create secret generic keycloak-password \
  --from-literal=password="password" \
  --namespace=staging

kubectl create secret generic postgres-password \
  --from-literal=postgresql-password="password" \
  --from-literal=postgresql-username="postgres" \
  --namespace=staging

kubectl create secret generic api-key \
  --from-literal=api-key="secret" \
  --namespace=staging