## Install redis high availability with helm

# dive in
```
gcloud config set project oecd-228113
gcloud config set container/cluster oecd
gcloud config set compute/region europe-west1-b
gcloud container clusters get-credentials oecd --region europe-west1-b
kubectl get pods -n staging -o wide
```

# setup
1. kubectl create -f namespace.json
1. create docker regcred secret from `regcred.sh`
1. create secrets from `secrets.sh`

> NO KEYCLOAK IN STAGING because qa frontend use staging backend, keycloak is in-between

# postgres with helm
1. `helm repo add bitnami https://charts.bitnami.com/bitnami`
1. `helm install postgres -f postgres/values.yaml bitnami/postgresql -n staging`

# keycloak with helm
1. `helm repo add codecentric https://codecentric.github.io/helm-charts`
1. `helm install keycloak -f keycloak/values.yaml codecentric/keycloak -n staging`
1. disable ssl:
  $  `kubectl exec -it -n staging keycloak-0 -- bash`
  $$ `cd keycloak/bin`
  $$ `./kcadm.sh update realms/master -s sslRequired=NONE --server http://localhost:8080/auth --realm master --user admin`

# helm help
- `helm delete <name> -n staging`
- `helm ls -a -n staging`
- `helm delete --purge <name> -n staging`

# solr
1. apply solr init `kubectl apply -f solr.init.yaml`
1. enter the container `kubectl exec -it solr-<hash> -n qa -- bash`
1. create the core `solr create sdmx-facet-search`
1. move the solr file `mv opt/solr/server/solr/*  /data`
1. note: /data should contains files and at least solr.xml
1. remove the worker
1. apply solr `kubectl apply -f solr.yaml`

## redis high availability with helm

- `$ kubectl apply -f redis.yml -n staging`
- remove redis workload
- `$ helm install --name redis -n staging stable/redis-ha`

## Backup Redis

* create snapshot + schedule based on redis disks in Compute Engine

## Restore Redis

* create a disk from snapshot in Compute Engine
* attach GCP disk to a  pod (see redis-bkp.yaml)
* stop redis-ha
* rename /data/dump.rdb
* copy redis-bkp:/data/dump.rdb to redis-ha:/data
* restart redis-ha

NB: above procedure is for a single redis instance, for redis-ha, we may have to copy dump.rdb to all instances
=> To be tested ....

# sfs
- curl -X DELETE http://sfs-qa-oecd.redpelicans.com/admin/dataflows?api-key=secret | jq
- curl -X DELETE http://sfs-qa-oecd.redpelicans.com/admin/config?api-key=secret | jq
- curl -X POST http://sfs-qa-oecd.redpelicans.com/admin/dataflows?api-key=secret -d {} | jq
- curl -X GET http://sfs-qa-oecd.redpelicans.com/admin/report?api-key=secret | jq
