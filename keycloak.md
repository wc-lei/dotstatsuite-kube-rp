# KEYCLOAK

## misc
- helm delete postgres -n staging
- helm delete keycloak -n staging

- helm install postgres -f postgres/values.yaml bitnami/postgresql -n staging
- helm install keycloak -f keycloak/values.yaml codecentric/keycloak -n staging

disable ssl:
- kubectl exec -it -n staging keycloak-0 -- bash
- cd keycloak/bin
- ./kcadm.sh update realms/master -s sslRequired=NONE --server http://localhost:8080/auth --realm master --user admin

## from 6.0.1 to 7.0.0

#### 0. dive in keycloak container (qa)
kubectl exec -it -n qa keycloak-0 -- bash
#### 1. export to /opt/jboss/keycloak/bin/qa-export.json
/opt/jboss/keycloak/bin/standalone.sh \
  -Dkeycloak.migration.action=export \
  -Dkeycloak.migration.provider=singleFile \
  -Dkeycloak.migration.file=qa-export.json \
  -Dkeycloak.migration.realmName=OECD \
  -Dkeycloak.migration.usersExportStrategy=REALM_FILE \
  -Djboss.http.port=8888 \
  -Djboss.https.port=9999 \
  -Djboss.management.http.port=7777
#### 2. exit container
exit
#### 3. export from keycloak container (qa) to .
kubectl cp -n qa keycloak-0:/opt/jboss/keycloak/bin/qa-export.json qa-export.json
#### 4. import to keycloak container (staging)
kubectl cp -n staging qa-export.json keycloak-0:/opt/jboss/keycloak/bin/qa-export.json
#### 5. dive in keycloak container (qa)
kubectl exec -it -n staging keycloak-0 -- bash
#### 6. import
cd /opt/jboss/keycloak/bin
./standalone.sh \
  -Dkeycloak.migration.action=import \
  -Dkeycloak.migration.provider=singleFile \
  -Dkeycloak.migration.file=qa-export.json \
  -Dkeycloak.migration.usersExportStrategy=REALM_FILE \
  -Djboss.http.port=8888 \
  -Djboss.https.port=9999 \
  -Djboss.management.http.port=7777
#### 7. restart
kubectl delete pod keycloak-0 -n staging
#### 8. delete existing helm
helm delete keycloak -n staging
#### 9. bump keycloak in values or do nothing (if tag is latest and pull policy is  always)
#### 10. install helm
helm install keycloak -f keycloak/values.yaml codecentric/keycloak -n staging
#### 11. check proper image
kubectl describe pod keycloak-0 -n staging